const monthNames = [
    "januari",
    "februari",
    "maart",
    "april",
    "mei",
    "juni",
    "juli",
    "augustus",
    "september",
    "oktober",
    "november",
    "december"
];
var date = new Date;

initialize();

function initialize() {
    displayDate();
    getDayContent(getDate());
}

function changeDate(delta) {
    date.setDate(date.getDate() + delta);
    initialize();
}

function getDate(dimension) {
    if ( dimension ) {
        switch(dimension) {
            case 'd':
                return date.getDate();
            case 'm':
                return date.getMonth()+1;
            case 'M':
                return monthNames[date.getMonth()];
            case 'y':
                return date.getFullYear();
            default:
                return false;
        }
    }
    return date
}

function displayDate() {
    var dateHTML = '<span class="day">' + getDate('d') + '</span>';
    dateHTML += '<span class="month">' + getDate('M') + '</span>';
    document.getElementById("date").innerHTML = dateHTML;
}

function getDayContent(date) {
    if (!date) {
        return false;
    }
    document.getElementById('content').innerHTML = "Dit is de boodschap voor deze dag";
}